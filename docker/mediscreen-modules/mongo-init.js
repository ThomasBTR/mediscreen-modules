db.createUser({
    user: 'user',
    pwd: 'user2022',
    roles: [{role: 'dbOwner', db: 'mediscreen-repository'}]
})

db = new Mongo().getDB('mediscreen-repository');


db.createCollection('medicalReview');

db.medicalReview.insertMany([
    {
        "patientId": 1,
        "notes": "Le patient déclare qu'il se sent fatigué pendant la journée\n Il se plaint également de douleurs musculaires\n Tests de laboratoire indiquant une microalbumine élevée",
        "date": "2022-09-26T10:20:20Z"
    },
    {
        "patientId": 1,
        "notes": "Le patient déclare qu'il ne se sent pas si fatigué que ça\n Fumeur, il a arrêté dans les 12 mois précédents\n Tests de laboratoire indiquant que les anticorps sont élevés",
        "date": "2022-10-26T10:20:20Z"
    },
    {
        "patientId": 2,
        "notes": "Le patient déclare qu'il ressent beaucoup de stress au travail\n Il se plaint également que son audition est anormale dernièrement",
        "date": "2022-08-26T10:25:20Z"
    },
    {
        "patientId": 2,
        "notes": "Le patient déclare avoir fait une réaction aux médicaments au cours des 3 derniers mois\n Il remarque également que son audition continue d'être anormale",
        "date": "2022-09-23T10:25:20Z"
    },
    {
        "patientId": 2,
        "notes": "Tests de laboratoire indiquant une microalbumine élevée",
        "date": "2022-10-18T10:25:20Z"
    },
    {
        "patientId": 2,
        "notes": "Le patient déclare que tout semble aller bien\n Le laboratoire rapporte que l'hémoglobine A1C dépasse le niveau recommandé\n Le patient déclare qu’il fume depuis longtemps",
        "date": "2022-10-26T10:25:20Z"
    },
    {
        "patientId": 3,
        "notes": "Le patient déclare qu'il fume depuis peu",
        "date": "2022-08-12T10:25:20Z"
    },
    {
        "patientId": 3,
        "notes": "Tests de laboratoire indiquant une microalbumine élevée",
        "date": "2022-09-27T10:25:20Z"
    },
    {
        "patientId": 3,
        "notes": "Le patient déclare qu'il est fumeur et qu'il a cessé de fumer l'année dernière\n Il se plaint également de crises d’apnée respiratoire anormales\n Tests de laboratoire indiquant un taux de cholestérol LDL élevé",
        "date": "2022-10-01T10:25:20Z"
    },
    {
        "patientId": 3,
        "notes": "Tests de laboratoire indiquant un taux de cholestérol LDL élevé",
        "date": "2022-10-25T10:25:20Z"
    },
    {
        "patientId": 4,
        "notes": "Le patient déclare qu'il lui est devenu difficile de monter les escaliers\n Il se plaint également d’être essoufflé\n Tests de laboratoire indiquant que les anticorps sont élevés\n Réaction aux médicaments",
        "date": "2022-07-25T10:25:20Z"
    },
    {
        "patientId": 4,
        "notes": "Le patient déclare qu'il a mal au dos lorsqu'il reste assis pendant longtemps",
        "date": "2022-08-25T10:25:20Z"
    },
    {
        "patientId": 4,
        "notes": "Le patient déclare avoir commencé à fumer depuis peu\n Hémoglobine A1C supérieure au niveau recommandé",
        "date": "2022-09-25T10:25:20Z"
    },
    {
        "patientId": 5,
        "notes": "Le patient déclare avoir des douleurs au cou occasionnellement\n Le patient remarque également que certains aliments ont un goût différent\n Réaction apparente aux médicaments\n Poids corporel supérieur au poids recommandé",
        "date": "2022-06-25T10:25:20Z"
    },
    {
        "patientId": 5,
        "notes": "Le patient déclare avoir eu plusieurs épisodes de vertige depuis la dernière visite.\n Taille incluse dans la fourchette concernée",
        "date": "2022-06-25T10:25:20Z"
    },
    {
        "patientId": 5,
        "notes": "Le patient déclare qu'il souffre encore de douleurs cervicales occasionnelles. \n Tests de laboratoire indiquant une microalbumine élevée \n Fumeur, il a arrêté dans les 12 mois précédents",
        "date": "2022-07-25T10:25:20Z"
    },
    {
        "patientId": 5,
        "notes": "Le patient déclare avoir eu plusieurs épisodes de vertige depuis la dernière visite. \n Tests de laboratoire indiquant que les anticorps sont élevés",
        "date": "2022-09-25T10:25:20Z"
    },
    {
        "patientId": 6,
        "notes": "Le patient déclare qu'il se sent bien\n Poids corporel supérieur au poids recommandé",
        "date": "2022-02-23T10:25:20Z"
    },
    {
        "patientId": 6,
        "notes": "Le patient déclare qu'il se sent bien",
        "date": "2022-08-12T10:25:20Z"
    },
    {
        "patientId": 7,
        "notes": "Le patient déclare qu'il se réveille souvent avec une raideur articulaire\n Il se plaint également de difficultés pour s’endormir\n Poids corporel supérieur au poids recommandé\n Tests de laboratoire indiquant un taux de cholestérol LDL élevé",
        "date": "2022-01-23T10:25:20Z"
    },
    {
        "patientId": 8,
        "notes": "Les tests de laboratoire indiquent que les anticorps sont élevés\n Hémoglobine A1C supérieure au niveau recommandé",
        "date": "2022-02-15T10:25:20Z"
    },
    {
        "patientId": 9,
        "notes": "Le patient déclare avoir de la difficulté à se concentrer sur ses devoirs scolaires\n Hémoglobine A1C supérieure au niveau recommandé",
        "date": "2021-01-15T10:25:20Z"
    },
    {
        "patientId": 9,
        "notes": "Le patient déclare qu'il s’impatiente facilement en cas d’attente prolongée\n Il signale également que les produits du distributeur automatique ne sont pas bons\n Tests de laboratoire signalant des taux anormaux de cellules sanguines",
        "date": "2021-09-14T10:25:20Z"
    },
    {
        "patientId": 9,
        "notes": "Le patient signale qu'il est facilement irrité par des broutilles\n Il déclare également que l'aspirateur des voisins fait trop de bruit\n Tests de laboratoire indiquant que les anticorps sont élevés",
        "date": "2022-02-18T10:25:20Z"
    },
    {
        "patientId": 10,
        "notes": "Le patient déclare qu'il n'a aucun problème",
        "date": "2021-09-01T10:25:20Z"
    },
    {
        "patientId": 10,
        "notes": "Le patient déclare qu'il n'a aucun problème\n Taille incluse dans la fourchette concernée\n Hémoglobine A1C supérieure au niveau recommandé",
        "date": "2021-09-14T10:25:20Z"
    },
    {
        "patientId": 10,
        "notes": "Le patient déclare qu'il n'a aucun problème\n Poids corporel supérieur au poids recommandé\n Le patient a signalé plusieurs épisodes de vertige depuis sa dernière visite",
        "date": "2022-03-09T10:25:20Z"
    },
    {
        "patientId": 10,
        "notes": "Le patient déclare qu'il n'a aucun problème\n Tests de laboratoire indiquant une microalbumine élevée",
        "date": "2022-05-19T10:25:20Z"
    }
]);